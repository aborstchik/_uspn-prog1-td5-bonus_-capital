#include <stdlib.h>
#include <stdio.h>

//Set to false to inhibit the user input for each variable.
#define USER_X 1
#define USER_R 1
#define USER_N 1


int main()
{
  //Initialization
  //____________________________________________________
  double x= 1000;
  double r= 1.2;
  long n= 12;

  long i= 0;
  double A= 1;
  //____________________________________________________


  //Inputs
  //____________________________________________________
  if(USER_X)
    {
      printf("x= ");
      scanf("%lg", &x);
    }
  if(USER_R)
    {
      printf("r= ");
      scanf("%lg", &r);
    }
  if(USER_N)
    {
      printf("n= ");
      scanf("%ld", &n);
    }
  //____________________________________________________

  
  //Computations
  //____________________________________________________
  for(i=0; i<n; i++)
    A*= (1+r);

  A*= x;
  //____________________________________________________


  //Post-process
  //____________________________________________________
  printf("A = %g€\n", A);
  //____________________________________________________

  

  return EXIT_SUCCESS;
}
