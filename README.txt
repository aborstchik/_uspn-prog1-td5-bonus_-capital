===============================================================
                         CAPITAL
===============================================================


Principe
_______________________________________________________________
Calcule le capital A produit par x euros placés au taux r au bout de n années selon la formule:
A = x(1+r)^n

Les valeurs de x, r et n sont données par l'utilisateur au lancement du programme.
_______________________________________________________________

Fichiers
__________________________________________________________________
algo_td5-bonus.txt: algorithme

prog_td5-bonus.c: code source du programme.

VERSION.txt: version actuelle du projet

Makefile
__________________________________________________________________


Compilation et exécution (Linux)
__________________________________________________________________
Pour compiler, ouvrir un terminal, se déplacer dans le répertoire du projet et entrer:
>make

Le programme peut être exécuté après compilation avec:
>./prog_td5-bonus.out
__________________________________________________________________


Informations supplémentaires
__________________________________________________________________
Réalisé dans le cadre d'un module de programmation de la licence d'informatique de l'Université Paris XIII.

GitLab:
https://gitlab.com/aborstchik/_uspn-prog1-td5-bonus_-capital.git
__________________________________________________________________

2021 - Creative Commons (CC0)
Alexis BORSTCHIK
