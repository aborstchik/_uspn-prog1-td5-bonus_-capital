CC=gcc
CFLAGS=-Wall

exe=prog_td5-bonus.out
obj=prog_td5-bonus.o

.PHONY: all
all: $(exe)

$(exe): $(obj)
	$(CC) $(CFLAGS) $^ -o $@

$(obj): %.o: %.c
	$(CC) $(CFLAGS) -c $< -o $@

.PHONY: clean
clean:
	$(RM) $(exe) $(obj)

.PHONY: gitadd
gitadd: clean
	git add *[!~#]
	git status
# add anything in the current directory execpt files that end with '~' or '#' (such as Emacs backup files).

